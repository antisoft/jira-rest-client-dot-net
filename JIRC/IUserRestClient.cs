﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserRestClient.cs" company="David Bevin">
//   Copyright (c) 2013 David Bevin.
// </copyright>
// // <summary>
//   https://bitbucket.org/dpbevin/jira-rest-client-dot-net
//   Licensed under the BSD 2-Clause License.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

using JIRC.Domain;
using JIRC.Domain.Input;

using ServiceStack.ServiceClient.Web;

namespace JIRC
{
    /// <summary>
    /// An interface for handling Users within JIRA.
    /// </summary>
    public interface IUserRestClient
    {
        /// <summary>
        /// Gets detailed information about the user.
        /// </summary>
        /// <param name="username">The login username for the user.</param>
        /// <returns>Detailed information about the user.</returns>
        /// <exception cref="WebServiceException">The specified username does not exist, or the caller does not have permission to view the users.</exception>
        User GetUser(string username);

        /// <summary>
        /// Gets detailed information about the user.
        /// </summary>
        /// <param name="userUri">The URI for the user resource.</param>
        /// <returns>Detailed information about the user.</returns>
        /// <exception cref="WebServiceException">The specified username does not exist, or the caller does not have permission to view the users.</exception>
        User GetUser(Uri userUri);

        /// <summary>
        /// Gets a list of all groups.
        /// </summary>
        /// <returns>A list of groups.</returns>
        IEnumerable<string> GetGroups();

        /// <summary>
        /// Adds user to group.
        /// </summary>
        /// <param name="groupKey">Key of group to add user.</param>
        /// <param name="userName">Name of user to add.</param>
        /// <exception cref="WebServiceException">The specified group does not exist, or user does not exist, or the calling user does not have permissions to add user to group, or user is already in a group.</exception>
        void AddToGroup(string groupKey, string userName);

        /// <summary>
        /// Creates a new user.
        /// </summary>
        /// <returns>Created user.</returns>
        BasicUser CreateUser(UserInput user);

        /// <summary>
        /// Updates a user.
        /// </summary>
        /// <param name="userUri">The URI of user to edit.</param>
        /// <param name="user">Information to update about a user.</param>
        /// <returns>Updated user.</returns>
        /// <exception cref="WebServiceException">If the calling user does not have permission to update users.</exception>
        User UpdateUser(Uri userUri, UserInputUpdate user);

        /// <summary>
        /// Updates a user.
        /// </summary>
        /// <param name="userName">Name of updating user.</param>
        /// <param name="user">Information to update about a user.</param>
        /// <returns>Updated user.</returns>
        /// <exception cref="WebServiceException">If the calling user does not have permission to update users.</exception>
        User UpdateUser(string userName, UserInputUpdate user);

        /// <summary>
        /// Gets a URI for user.
        /// </summary>
        /// <param name="userName">Name of user.</param>
        /// <returns></returns>
        Uri GetUri(string userName);
    }
}
