﻿using JIRC.Domain.Input;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JIRC.Internal.Json.Gen
{
    internal class UserInputJsonGenerator
    {
        internal static JsonObject Generate(UserInput userInput)
        {
            if (userInput == null)
            {
                throw new ArgumentNullException("userInput");
            }

            // those are required fields
            var jsonObject = new JsonObject()
            {
                { "name", userInput.Name },
                { "emailAddress", userInput.EmailAddress },
                { "displayName", userInput.DisplayName }
            };

            if (userInput.Password != null)
            {
                jsonObject.Add("password", userInput.Password);
            }
            else
            {
                jsonObject.Add("notification", true.ToString());
            }

            return jsonObject;
        }

        internal static JsonObject Generate(UserInputUpdate userInput)
        {
            var jsonObject = new JsonObject()
            {
                { "name", userInput.Name }
            };

            if (userInput.DisplayName != null)
            {
                jsonObject.Add("displayName", userInput.DisplayName);
            }

            if (userInput.EmailAddress != null)
            {
                jsonObject.Add("emailAddress", userInput.EmailAddress);
            }
            
            return jsonObject;
        }
    }
}
