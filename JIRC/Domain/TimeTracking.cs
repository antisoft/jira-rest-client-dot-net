﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimeTracking.cs" company="Saritasa">
//   Copyright (c) 2017 Saritasa.
// </copyright>
// <summary>
//   https://bitbucket.org/dpbevin/jira-rest-client-dot-net
//   Licensed under the BSD 2-Clause License.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

using JIRC.Extensions;

namespace JIRC.Domain
{
    /// <summary>
    /// Representation of issue time tracking information.
    /// </summary>
    public class TimeTracking
    {
        public int OriginalEstimate { get; internal set; }

        /// <summary>
        /// Original estimate in seconds.
        /// </summary>
        public int OriginalEstimateSeconds { get; internal set; }

        /// <summary>
        /// Remaining issue estimate as formatted string.
        /// </summary>
        public string RemainingEstimate { get; internal set; }

        /// <summary>
        /// Remaining issue estimate in seconds.
        /// </summary>
        public int RemainingEstimateSeconds { get; internal set; }

        /// <summary>
        /// Time spent on issue as formatted string.
        /// </summary>
        public string TimeSpent { get; internal set; }

        /// <summary>
        /// Time spent on issue in seconds.
        /// </summary>
        public int TimeSpentSeconds { get; internal set; }
    }
}
