﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JIRC.Domain
{
    internal class ProjectKeysInfo
    {
        internal ProjectKeysInfo(IEnumerable<string> projectKeys)
        {
            ProjectKeys = projectKeys;
        }

        public IEnumerable<string> ProjectKeys { get; set; }
    }
}
