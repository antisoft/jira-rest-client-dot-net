﻿using System;

namespace JIRC.Domain
{
    public class Worklog
    {
        public Worklog(Uri self, string id, User author, User updateAuthor, string comment, DateTime created, DateTime updated, DateTime started, string timeSpent, int timeSpentSeconds)
        {
            Self = self;
            Id = id;
            Author = author;
            UpdateAuthor = updateAuthor;
            Comment = comment;
            Created = created;
            Updated = updated;
            Started = started;
            TimeSpent = timeSpent;
            TimeSpentSeconds = timeSpentSeconds;
        }

        public Uri Self { get; private set; }

        public string Id { get; private set; }

        public User Author { get; private set; }

        public User UpdateAuthor { get; private set; }

        public string Comment { get; private set; }

        public DateTime Started { get; private set; }

        public DateTime Created { get; private set; }

        public DateTime Updated { get; private set; }

        public string TimeSpent { get; private set; }

        public int TimeSpentSeconds { get; private set; }
    }
}
