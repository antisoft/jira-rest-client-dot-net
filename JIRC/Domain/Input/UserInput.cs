﻿using System.Collections.Generic;

namespace JIRC.Domain.Input
{
    public class UserInput
    {
        public string DisplayName { get; set; }

        public string EmailAddress { get; set; }

        public string Name { get; set; }

        public string Password { get; set; }
    }
}
