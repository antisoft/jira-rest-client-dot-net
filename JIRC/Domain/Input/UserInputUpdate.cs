﻿
namespace JIRC.Domain.Input
{
    public class UserInputUpdate
    {
        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string EmailAddress { get; set; }
    }
}
