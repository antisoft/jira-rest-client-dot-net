﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JiraUserRestClient.cs" company="David Bevin">
//   Copyright (c) 2013 David Bevin.
// </copyright>
// // <summary>
//   https://bitbucket.org/dpbevin/jira-rest-client-dot-net
//   Licensed under the BSD 2-Clause License.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;

using JIRC.Domain;
using JIRC.Domain.Input;
using JIRC.Extensions;
using JIRC.Internal.Json.Gen;

using ServiceStack.ServiceClient.Web;
using ServiceStack.Text;

namespace JIRC.Clients
{
    /// <summary>
    /// The REST client for Users in JIRA.
    /// </summary>
    internal class JiraUserRestClient : IUserRestClient
    {
        /// <summary>
        /// The URI resource used when searching for users.
        /// </summary>
        internal const string UserAssignableSearchUriPrefix = "user/assignable/search";

        private const string UserUriPrefix = "user";

        private const string GroupPickerUriPrefix = "groups/picker";

        private const string AddToGroupPrefix = "group/user";

        private readonly JsonServiceClient client;

        /// <summary>
        /// Initializes a new instance of the User REST client.
        /// </summary>
        /// <param name="client">The JSON client that has been set up for a specific JIRA instance.</param>
        public JiraUserRestClient(JsonServiceClient client)
        {
            this.client = client;
        }

        /// <summary>
        /// Gets detailed information about the user.
        /// </summary>
        /// <param name="username">The login username for the user.</param>
        /// <returns>Detailed information about the user.</returns>
        /// <exception cref="WebServiceException">The specified username does not exist, or the caller does not have permission to view the users.</exception>
        public User GetUser(string username)
        {
            var qb = new UriBuilder(client.BaseUri.AppendPath(UserUriPrefix));
            qb.AppendQuery("username", username);
            qb.AppendQuery("expand", "groups");

            return GetUser(qb.Uri);
        }

        /// <summary>
        /// Gets detailed information about the user.
        /// </summary>
        /// <param name="userUri">The URI for the user resource.</param>
        /// <returns>Detailed information about the user.</returns>
        /// <exception cref="WebServiceException">The specified username does not exist, or the caller does not have permission to view the users.</exception>
        public User GetUser(Uri userUri)
        {
            return client.Get<User>(userUri.ToString());
        }

        /// <summary>
        /// Gets a list of all groups.
        /// </summary>
        /// <returns>A list of groups.</returns>
        public IEnumerable<string> GetGroups()
        {
            var qb = new UriBuilder(client.BaseUri.AppendUrlPathsRaw(GroupPickerUriPrefix));
            qb.AppendQuery("maxResults", "500");

            var response = client.Get<JsonObject>(qb.Uri.ToString());

            var groups = response.Get<IEnumerable<JsonObject>>("groups");

            return groups.Select(a => a.Get<string>("name"));
        }

        /// <summary>
        /// Creates a new user
        /// </summary>
        /// <returns>Details of the created user.</returns>
        /// <exception cref="WebServiceException">The input is invalid (e.g. missing required fields, invalid field values, and so forth), or if the calling user does not have permission to create the user.</exception>
        public BasicUser CreateUser(UserInput user)
        {
            var json = UserInputJsonGenerator.Generate(user);
            return client.Post<BasicUser>("user", json);
        }

        /// <summary>
        /// Updates a user.
        /// </summary>
        /// <param name="userUri">The URI of user to edit.</param>
        /// <param name="user">Information to update about a user.</param>
        /// <returns>Updated user.</returns>
        /// <exception cref="WebServiceException">If the calling user does not have permission to update users.</exception>
        public User UpdateUser(Uri userUri, UserInputUpdate user)
        {
            var json = UserInputJsonGenerator.Generate(user);
            return client.Put<User>(userUri.ToString(), json);
        }

        /// <summary>
        /// Updates a user.
        /// </summary>
        /// <param name="userName">Name of updating user.</param>
        /// <param name="user">Information to update about a user.</param>
        /// <returns>Updated user.</returns>
        /// <exception cref="WebServiceException">If the calling user does not have permission to update users.</exception>
        public User UpdateUser(string userName, UserInputUpdate user)
        {
            var qb = new UriBuilder(client.BaseUri.AppendUrlPathsRaw(UserUriPrefix));
            qb.AppendQuery("username", userName);

            return UpdateUser(qb.Uri, user);
        }

        /// <summary>
        /// Adds user to group.
        /// </summary>
        /// <param name="groupKey">Key of group to add user.</param>
        /// <param name="userName">Name of user to add.</param>
        /// <exception cref="WebServiceException">The specified group does not exist, or user does not exist, or the calling user does not have permissions to add user to group, or user is already in a group.</exception>
        public void AddToGroup(string groupKey, string userName)
        {
            var qb = new UriBuilder(client.BaseUri.AppendUrlPathsRaw(AddToGroupPrefix));
            qb.AppendQuery("groupname", groupKey);

            var json = new JsonObject()
            {
                { "name", userName }
            };
            client.Post<JsonObject>(qb.Uri.ToString(), json);
        }

        /// <summary>
        /// Gets a URI for user.
        /// </summary>
        /// <param name="userName">Name of user.</param>
        /// <returns></returns>
        public Uri GetUri(string userName)
        {
            var qb = new UriBuilder(client.BaseUri.AppendUrlPathsRaw(UserUriPrefix));
            qb.AppendQuery("username", userName);
            return qb.Uri;
        }
    }
}
